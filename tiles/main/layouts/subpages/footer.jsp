<%-- 
    Document   : footer
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<footer class="bottom-fix">
  <div class="row">
    <div class="col-md-12 text-center">
      <span>©iZSearch. Search it easy!</span>
    </div>
  </div>
</footer>
