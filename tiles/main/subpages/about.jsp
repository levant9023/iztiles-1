<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/about/styles/about-us.css" />
<c:set var="title" value="About" scope="request" />
<link type="text/css" rel="stylesheet" href="${baseURL}/resources/js/production/plugins/flexislider/flexslider.css" />
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/flexislider/jquery.flexslider-min.js"></script>

<div class="header">
	<div class="container">
		<a class="logo" href="/" style="margin-bottom:-6px;">
			<img src="${baseURL}/resources/img/logo.svg" alt="logo">
			<br>© iZSearch
		</a>
		<div class="welcome">
			<div class="main-video">
				<%--	<iframe src="https://www.youtube.com/embed/ScGkfzE1X38" frameborder="0" allowfullscreen></iframe>--%>
                <%--	<p>iZSearch -  search engine without distracting ads, spam or tracking.</p> --%>
				<iframe src="https://www.youtube.com/embed/gD6ftip3C98" frameborder="0" allowfullscreen></iframe>
				<p>iZSearch -  Less Ads & Spam, More Features & Privacy. </p>

			</div>
		</div>
	</div>
	<%--
	<div class="show-benefits row">
		<div class="col-sm-3">
			<a href="/about.html#private-search">
				<img src="${baseURL}/resources/img/subpages/privacy.png" alt="Privacy">
				<h4>Private Search</h4>
				<div class="col-md-3">
					<iframe src="https://www.youtube.com/embed/-nple-4M80s" frameborder="0" allowfullscreen></iframe>
				</div>
				<p>iZSearch offers a search service which does not retain or share any of your personal information. iZSearch does not download “cookies” onto people’s devices. It does not register the “IP address” which pinpoints a users computer.</p>
			</a>
		</div>

		<div class="col-sm-3">
			<a href="/about.html#ads">
				<img src="${baseURL}/resources/img/subpages/ads.png" alt="Less ads">
				<h4>Less spam</h4>
				<div class="col-md-3">
					<iframe src="https://www.youtube.com/embed/GYElhlwhk5M" frameborder="0" allowfullscreen></iframe>
				</div>
				<p>By default, iZSearch shows only minimal ads at the bottom of the search results page. iZSearch does not sell data about you to third parties, including advertisers and data brokers.</p>
			</a>
		</div>

		<div class="col-sm-3">
			<a href="/about.html#features">
				<img src="${baseURL}/resources/img/subpages/features.png" alt="Features">
				<h4>More features</h4>
				<div class="col-md-3">
					<iframe src="https://www.youtube.com/embed/TBEAo5KTLr0" frameborder="0" allowfullscreen></iframe>
				</div>
				<p> iZSearch has everything you expect a search engine to have plus a broad range of advanced features including images, videos, news and thematic search pages, all this without promoting spam results such as social networks (e.g. Google+), mail (e.g. Gmail) or other irrelevant results.</p>
			</a>
		</div>

		<div class="col-sm-3">
			<a href="/about.html#vertical">
				<img src="${baseURL}/resources/img/subpages/search.png" alt="Vertical search">
				<h4>Thematic search</h4>
				<div class="col-md-3">
					<iframe src="https://www.youtube.com/embed/gD6ftip3C98" frameborder="0" allowfullscreen></iframe>
				</div>
				<p>Thematic (or Vertical) search focuses on a specific area of human interest. iZSearch covers over 50 popular verticals and provides thematic search pages (and API access) for Science, Technology, Health, Food, Music, Travel, Sports, Politics and many other areas of human interest.</p>
			</a>
		</div>
	</div>  --%>
</div>

<div class="container">

	<div id="features">
		<h1>iZSearch is Privacy focused, clean from the Ads and Spam web search engine.</h1>
		<div id="discover">
			<h6>iZSearch Main page</h6>
			<p>Main page of iZSearch can be expanded to see headlines news and feeds from over 40 areas of human interest including Top News and Blogs, Science and Technology, Politics, Sports, Food, Travel, Business, and Health, among many others.</p>
			<div class="flex">
				<ul class="slides">
					<li><img src="${baseURL}/resources/img/about/features/main_down.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/main_up.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/main_popular.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/main_trending.jpg"></li>
				</ul>
			</div>
		</div>
		<div id="results">
			<h6>Great Results</h6>
			<p>iZSearch has everything you expect a search engine to have, including images, videos, news and places, all while respecting your privacy.</p>
			<img src="${baseURL}/resources/img/about/features/mainwndow.png">
		</div>
		<hr class="lws">

		<h6>Search images, videos and places</h6>
		<p>iZSearch searches for images, videos, places and all kinds of media information.</p>
		<div class="flex">
			<ul class="slides">
				<li><img src="${baseURL}/resources/img/about/features/images.png"></li>
				<li><img src="${baseURL}/resources/img/about/features/videos.png"></li>
				<li><img src="${baseURL}/resources/img/about/features/map.png"></li>
			</ul>
		</div>

		<hr class="lws">

		<div id="all_results">
			<h6>Vertical pages</h6>
			<p>iZSearch searches for thematic news, topics, stories, articles, and reviews from over 40 areas of human interest.</p>
			<div class="flex">
				<ul class="slides">
					<li><img src="${baseURL}/resources/img/about/features/filter_all_1.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/filter_all_2.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/filter_all_3.jpg"></li>
				</ul>
			</div>
		</div>

		<div id="trending">
			<h6>Trending and Popular news and topics</h6>
			<p> Latest and Breaking news, together with Trending and Popular topics for a longer period of time are shown for each category.</p>
			<div class="flex">
				<ul class="slides">
					<li><img src="${baseURL}/resources/img/about/features/filter_trending_1.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/filter_trending_2.jpg"></li>
					<li><img src="${baseURL}/resources/img/about/features/filter_trending_3.jpg"></li>
				</ul>
			</div>
		</div>

		<hr class="lws">

		<div id="vertical">
			<h6>Thematic (Vertical) search</h6>
			<p>Over 40 Popular themes/verticals covered: Health, Food, Science, Tech, Music, Travel, Sports, Politics and many other.</p>
			<div class="flex">
				<ul class="slides">
					<li><img src="${baseURL}/resources/img/about/features/vertical1.png"></li>
					<li><img src="${baseURL}/resources/img/about/features/vertical2.png"></li>
					<li><img src="${baseURL}/resources/img/about/features/vertical3.png"></li>
				</ul>
			</div>
		</div>

		<hr class="lws">

		<h6>Article page</h6>
		<p>With extended Description, Similar articles and Share options.</p>
		<img src="${baseURL}/resources/img/about/features/article_page.jpg">

		<hr class="lws">

		<h6>Mobile version</h6>
		<p>iZSearch automatically detects your mobile device (such as iphone, ipad, etc.) and shows correct version of the site.</p>
		<img src="${baseURL}/resources/img/about/features/mobile.png">

		<hr class="lws">

		<h6>Page previews</h6>
		<p>Page previews allow you to qickly see the web page preview and check if the page is really what you want. Just roll over the title of the page. The feature that greatly saves your time!</p>
		<img src="${baseURL}/resources/img/about/features/pageview.png">

		<hr class="lws">

		<h6>Quick Buttons</h6>
		<p>Quick Buttons help you search directly on thousands of other popular web sites. A search for shoes returns Quick Butons like Amazon, Zappos, Gap etc. that will take you right to a search for shoes on Amazon, Zappos, Gap etc.</p>
		<img src="${baseURL}/resources/img/about/features/quickbuttons.png">

		<hr class="lws">

		<h6>Topics</h6>
		<p>Topics - are thematic groups of visual buttons related to your search. For example for diabetes search you get Health, Health&Fitness and other themes, whereas for iphone 6s you get - Shopping, Gadgets, Tech reviews, News, Videos and other topics.</p>
		<img src="${baseURL}/resources/img/about/features/topics.png">

		<hr class="lws">

		<h6>Advanced search</h6>
		<p>Advanced search - allows you to search using search operators and other punctuation to get more specific search results. It allows you to narrow down your search results for complex searches. For example, you can “Find wikipedia pages talking about drugs affecting breast cancer on the gene or DNA level”: (drugs "breast cancer" (gene OR DNA) site: wikipedia.org) Additionally it allows you specify the types of results you to be returned (not only web pages), for example: Emails, Names, Cities, Countries, etc.</p>
		<img src="${baseURL}/resources/img/about/features/advanced.png">

		<hr class="lws">

		<h6>Interaction with User Community</h6>
		<p>You can report features, bugs or any problems live on the web site</p>
		<div class="flex">
			<ul class="slides">
				<li><img src="${baseURL}/resources/img/about/features/bugs.png"></li>
				<li><img src="${baseURL}/resources/img/about/features/brep.png"></li>
			</ul>
		</div>

		<hr class="lws">

		<h6>Real Privacy</h6>
		<p>We offer a search service which does not retain or share any of your personal information. iZSearch does not download “cookies” onto people’s devices. It does not register the “IP address” which pinpoints a users computer. It does not "filter" search results. That is distinct from what other companies do. We are not anonymizing/encrypting the data. We actually throw it away - everything related to the user and anything that is personally identifiable.</p>
		<img src="${baseURL}/resources/img/about/features/privacy.png">

		<hr class="lws">

		<div id="ads">
			<h6>Fewer adds and commercials</h6>
			<p>And the last but not the least feature: we do really have much fewer Ads and clutter on the search results page then other search engines do. By default, iZSearch shows only minimal ads at the bottom of the search results page. iZSearch does not sell data about you to third parties, including advertisers and data brokers.</p>
			<img src="${baseURL}/resources/img/about/features/ads.png">
		</div>
	</div>

	<div id="lsa-text" class="row">
		<div class="col-sm-12">
			<h1>FAQ</h1>
		</div>
		<div class="col-sm-6">
			<ul class="list-group">
				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/whatis.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is iZSearch?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is a general purpose search engine that is intended to be your starting place when searching the Internet. Use it to <b>get way more instant answers</b>, way less spam and real privacy, which we believe adds up to a much better overall search experience...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/izsearch-name.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Name?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The name iZSearch is derived from Easy Search, but it's not a metaphor. If you're wondering how you would turn that into a verb... Search it easy!</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/agent-handle.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How does iZSearch handle user agents?</h4>
						</div>
						<div class="qbody issue-body">
							<p>A user agent identifies your browser (Firefox, Internet Explorer, Safari, etc.) and provides certain system details to the websites you visit. Your web browser automatically sends this information to every webpage you visit...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/search-engine-in-chrome.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How do I install iZSearch as my default search engine in Chrome?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The Chrome web browser has a unique way of performing default searches. Rather than give you a search box in the upper right corner of your browser to perform searches, Chrome has you type your search term(s) directly into the address (URL) bar...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/what-is-ssl.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is a SSL/TLS Server Certificate?</h4>
						</div>
						<div class="qbody issue-body">
							<p>Whenever you use your web browser to communicate securely over the Internet (i.e. for banking, private search through iZSearch, etc.) you are using a technology known as SSL (Secure SocketLayer) or TLS (Transport Layer Security)...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/getting-cookies.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Am I still getting cookies even though I am using your service?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies of any kind.</p>
							<p>There are two ways you could pick up a cookie when using our service:</p>
							<p>The first is an anonymous iZSearch Settings cookie that you can choose to accept...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/mylocation.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How do you know my language or location if you don't store information about me?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not record IP addresses. We determine which browser you are using (Internet Explorer, Firefox, Safari, etc.) so we can customize the link on our home page that allows you to add iZSearch to your browser...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/cookie-policy.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is iZSearch's cookie policy?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies, and we do not access or review cookies placed on your browser by other websites.</p>
							<p>If you use the iZSearch Settings page and save your preferences...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/server-logs.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What exactly gets recorded in your server logs? (IP Address? User Agent?)</h4>
						</div>
						<div class="qbody issue-body">
							<p>We have customized our Web server software so that it does not log any IP addresses or user agents. It writes 0.0.0.0 in lieu of an IP address and leaves a blank space for the user agent. No unique IP address is ever stored on our systems...</p>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<ul class="list-group">
				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/izsearch-architecture.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is Architecture of iZSearch?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is coded in Java, served via nginx, FastCGI and memcached, running on FreeBSD and Ubuntu via daemontools. We both run our own servers across the world. Your connection generally goes to the closest regional server available to your area...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/privacy-protection.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Does iZSearch add privacy protection to other sites I visit?</h4>
						</div>
						<div class="qbody issue-body">
							<p>No, iZSearch does not add privacy protection to websites you visit. The websites you visit are third-party websites and are not part of iZSearch. iZSearch protects your privacy as you search for information...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/windows-compatible.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Is iZSearch compatible with Windows 10 / Edge?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is now compatible with Windows 10/Edge.</p>
							<p>To add iZSearch as a default search engine in Edge, please open your Edge browser, and follow the instructions given here...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/collection-policy.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>What is your data collection policy? What do you record?</h4>
						</div>
						<div class="qbody issue-body">
							<p>We don't collect any personal information on our visitors.</p>
							<p>When you use iZSearch, we do not record your IP address, we do not record which browser you are using (Internet Explorer, Safari, Firefox, etc.), we do not record your...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/favorite-websites.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Can iZSearch store my favorite websites and bookmarks?</h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch makes a point of not storing your searches and not recording the websites you visit.</p>
							<p>However  iZSearch has a big library of popular resources that you can directly serach...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/extended-validation.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Does iZSearch have an Extended Validation (EV) SSL certificate?</h4>
						</div>
						<div class="qbody issue-body">
							<p>Traditional SSL certificates provide basic information about the certificate holder, as well as the organization that issued the certificate. However, the amount of validation done to confirm the identify of the organization requesting the certificate...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/encryption-protect.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>How does HTTPS encryption protect me? Does it keep my ISP from seeing me?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The "https (SSL or "secure socket layer"/TLS "transport layer security") encrypted" connection you make with iZSearch prevents your Internet service provider (ISP) from seeing what you're searching for while you are on iZSearch...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/background-information.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Where can I find more background information on privacy issues?</h4>
						</div>
						<div class="qbody issue-body">
							<p>The websites of various privacy organizations are a great starting point for locating an abundance of background information on this subject...</p>
						</div>
					</div>
				</li>

				<li class="list-group-item" onclick="location.href='${baseURL}/subpages/.html';">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Where is the site map?</h4>
						</div>
						<div class="qbody issue-body">
							<p>You can find sitemap by following this link: <a href="${baseURL}/subpages/sitemap.html">iZSearch sitemap</a></p>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div id="private-search">
		<h1>Terms</h1>
		<h3>Personal Information</h3>
		<p>iZSearch does not retain or share personal information with any third party, including its provider(s) or sponsored search results.</p>
		<p>Some results page may include third party products or services such a small number of sponsored links to generate revenue and cover operational costs. Those links are retrieved from reputed platforms such as Google AdSense, eBay and others. However, all these links are safe for the user, because they do not transmit any personal information to the site owners. Furthermore, all of these links are processed first by engine iZSearch, which first clear it and after proxy to сorresponding resources.</p>

		<h3>IP Addresses</h3>
		<p>The IP address that is associated with your search will not be ever saved or shared.</p>

		<h3>Cookies</h3>
		<p>Cookies is a small piece of data sent from a website and stored in your web browser (computer's hard drive) when you visiting website.</p>
		<p>iZSearch use a small count of cookies that are only necessary for correctly working a webpage GUI widgets.</p>
		<p>You can choose to accept or refuse cookies by changing the settings of your browser. However you should be aware that this action can afect properly functioning of some javascript elements on the webpage.</p>
		<p>Session cookies expire when you close the browser. Persistent such (flash, images etc.) have typical expiration dates ranging from two months up to a couple of years.</p>

		<h3>Terms</h3>
		<p>This Agreement will enter into force from the moment of your first contact with the iZSearch.com and start using any of the services.</p>
		<p>You accept irrevocably and unconditionally and adhere to these Terms as you are accessing, browsing or using the Site and/or any of the Services.</p>
		<p>You also confirm that you agree to be bound by this Agreement without any exemptions, limitations and exclusions, and any and all provisions of this Agreement shall be enforceable to the fullest extent against you.</p>
		<p>You will accept the terms of this Agreement if you access the Site or use any of the Services on behalf a business. In this way iZSearch, Company, their officers, agents, employees and Partners were hold harmless and indemnify from any claim, suit or action arising from or related to the use of the Site and Services or violation of the Agreement, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees.</p>
		<p>The User thereby understands and agrees that  iZSearch  owns all copyrights, patent, trademark, know-how and other intellectual property rights (“IPR”) in relation to the Site.</p>
		<p>You should know that the sponsors or advertisers, who provide that content to iZSearch, have intellectual property rights to protect this content  presented to you, as part of the Site or Services. If you haven’t been specifically told by iZSearch or by the owners of that Content, in a separate agreement to modify, rent, lease, loan, sell, distribute or create derivative works based on this content (either in whole or in part), you may not do this.</p>
		<p>The right to review, filter, modify, refuse or remove any or all content from any Service is reserved by iZSearsh. For some of the Services, iZSearch may provide tools to filter out explicit sexual, obscene or indecent content. In addition, the access to material that you may find objectionable, may be limited by the commercially available services and software.</p>
		<p>Some of the Services may display advertisements and promotions, being supported by the advertising revenue. The content of information stored on the Services may be targeted to these advertisements, queries made through the Services or other information.</p>
		<p>The Services may include hyperlinks to other web sites or content or resources. These web sites or resources which are provided by companies or persons other than iZSearch, aren’t controlled by iZSearch.</p>
		<p>If you do not accept the Terms, you may not use the Site and Services.</p>
		<p>iZSrearch may modify these Terms, unilaterally without any notice.</p>
	</div>

	<div class="container">
		<div class="team row">
			<div class="col-sm-12">
				<h1>Our Team</h1>
			</div>

			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/sergey_egorov.png" alt="Sergey Egorov CEO">
				<h4>Sergey Egorov</h4>
				<p>CEO</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/kenneth_abeloe.png" alt="Kenneth Abeloe COO">
				<h4>Kenneth Abeloe</h4>
				<p>COO</p>
			</div>
			<hr class="lws">
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/iulia_dubinina.png" alt="Iulia Dubinina programmer">
				<h4>Iulia Dubinina</h4>
				<p>Lead programmer</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/no_image.png" alt="Dmitriy Dubinin programmer">
				<h4>Dmitriy Dubinin</h4>
				<p>Programmer</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/ghenadie_melniciuc.png" alt="Ghenadie Melniciuc programmer">
				<h4>Ghenadie Melniciuc</h4>
				<p>Programmer</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/alexander_shevtsov.png" alt="Alexander Shevtsov Programmer">
				<h4>Alexander Shevtsov</h4>
				<p>Programmer</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/mihail_toderashco.png" alt="Mihail Toderashco Programmer">
				<h4>Mihail Toderashco</h4>
				<p>Programmer</p>
			</div>
			<hr class="lws">
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/iulian_pojar.png" alt="Iulian Pojar System Administrator">
				<h4>Iulian Pojar</h4>
				<p>System Administrator</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/no_image.png" alt="Turcu Vasile Front-End Developer">
				<h4>Turcu Vasile</h4>
				<p>Front-End Developer</p>
			</div>
			<hr class="lws">
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/ana_verijac.png" alt="Ana Verijac Marketing">
				<h4>Ana Verijac</h4>
				<p>Marketing</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/no_image.png" alt="Victor Verijac Marketing">
				<h4>Victor Verijac</h4>
				<p>Marketing</p>
			</div>

			<div class="col-sm-12">
				<h2>Advisory Board</h2>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/no_image.png" alt="Tom Garcia (Strategic Relations) - TCA Member">
				<h4>Tom Garcia</h4>
				<p>(Strategic Relations) – TCA Member</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/team/harry_friedman.jpg" alt="Harry Friedman (Strategic Development) - TCA member">
				<h4>Harry Friedman</h4>
				<p>(Strategic Development) - TCA member</p>
			</div>
			<div class="col-sm-3">
				<img src="${baseURL}/resources/img/about/no_image.png" alt="Greg Hermanson (IP) - Knobbe Martens, Partner">
				<h4>Greg Hermanson</h4>
				<p>(IP) – Knobbe Martens, Partner</p>
			</div>
		</div>
	</div>

	<div class="contact">
		<h2>Address</h2>

		<dl class="dl-horizontal">
			<dt>Company name</dt>
			<dd>izsearch.com</dd>

			<dt>Address</dt>
			<dd>2711 CENTERVILLE RD, Suite 400</dd>

			<dt>City</dt>
			<dd>Wilmington</dd>

			<dt>Country</dt>
			<dd>US</dd>

			<dt>State</dt>
			<dd>Delaware</dd>

			<dt>Zip</dt>
			<dd>19808</dd>

			<dt>Phone</dt>
			<dd>(858) 480-9531</dd>

			<dt>Email</dt>
			<dd>business@izsearch.com</dd>
		</dl>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('.flex').flexslider({
			direction: 'horizontal',
			slideshow: true,
			slideshowSpeed: 4000,
			animation: 'fade',
			animationSpeed: 1000,
			animationLoop: true,
			keyboard: false,
			controlNav: false,
			directionNav: false
		});
	});
</script>
