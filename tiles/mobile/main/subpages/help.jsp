<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var="title" value="less ads" scope="request"/>

<div class="container-fluid">
	<div class="header text-center">
		<a class="logo" href="/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"></a>
		<h1>iZSearch A&Q</h1>
	</div>
	<div id="arts">
		<div id="lsa-text">
			<div class="motto-title">
				<h1 class="grob">Have a Question? Check for answer on this page.</h1>
			</div>
			<ul class="list-group lg-left">
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/whatis.html">What is iZSearch?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is a general purpose search engine that is intended to be your starting place when searching the Internet. Use it to <b>get way more instant answers</b>, way less spam and real privacy, which we believe adds up to a much better overall search experience. See our about page for more information.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/izsearch-name.html">Name?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>The name iZSearch is derived from Easy Search, but it's not a metaphor. If you're wondering how you would turn that into a verb... Search it easy!</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/agent-handle.html">How does iZSearch handle user agents?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>A user agent identifies your browser (Firefox, Internet Explorer, Safari, etc.) and provides certain system details to the websites you visit. Your web browser automatically sends this information to every webpage you visit. The user agent data helps websites tailor their pages and content to your browser and platform. You can refer to Wikipedia and Microsoft for additional information on user agents here: <a href="https://en.wikipedia.org/wiki/User_agent">https://en.wikipedia.org/wiki/User_agent</a><br><a href="http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx">http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx</a> iZSearch <b>does not store your user agent data</b>. That is very important, because user agent data can be used to "fingerprint" your browser and identify you. We never collect or store add-on details either, as they are specific enough to make browser fingerprinting possible. The only information we log is total search numbers conducted per language, as an aggregate figure.</p>
							<p>iZSearch does not store your user agent data. That is very important, because user agent data can be used to "fingerprint" your browser and identify you. We never collect or store add-on details either, as they are specific enough to make browser fingerprinting possible. The only information we log is total search numbers conducted per language, as an aggregate figure.</p>
							<p>iZSearch uses your user agent as follows: To customize the "Add to... " message on our home page. iZSearch uses the user agent string to customize the home page(www.iZSearch.com) where it provides instructions to add iZSearch to your browser. (That's under the search box where it says "Add to Internet Explorer," "Add to Firefox," "Add to Safari," etc.) Those directions differ from browser to browser, so we tailor the instructions you see.</p>
							<p>User agent data helps us prevent our service from being "scraped" by automatic programs or bots. One way we can identify bots is that they often do not send user-agents, while a normal user of our service would. This is why we do not allow access to iZSearch by visitors who do not transmit a user agent.</p>
							<p>A number of browsers also offer add-ons so that you can "fake" your user agent data thereby enhancing your privacy and overcoming the perceived scraping obstacle.</p>
							<p>A final word about fingerprinting techniques: iZSearch does not attempt to "fingerprint" and track visitors, under any circumstances. iZSearch opposes the use of so-called "canvas fingerprinting" and evercookies, and does not use these or any other techniques to gather and retain data about you, your computing environment, or your browsing habits. If you are concerned about the use of canvas fingerprinting or evercookies, our Proxy can protect you from both when browsing web sites.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/search-engine-in-chrome.html">How do I install iZSearch as my default search engine in Chrome?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>The Chrome web browser has a unique way of performing default searches. Rather than give you a search box in the upper right corner of your browser to perform searches, Chrome has you type your search term(s) directly into the address (URL) bar.</p>
							<p>To set iZSearch as your Chrome browser default search engine:</p>
							<ol>
								<li>Click on the three horizontal bars at the top right in your Chrome browser and select Settings.</li>
								<li>Now locate the Search area and click Manage search engines...</li>
								<li>In the popup, scroll to the bottom of the list of Other search engines, and fill out the three fields:
									<ul>
										<li>Add a new search engine: iZSearch</li>
										<li>Keyword: iZSearch.com</li>
										<li>URL with %s in place of query: <a href="https://iZSearch.com/do/search?query=%s&cat=web&pl=chrome&language=english">https://iZSearch.com/do/search?query=%s</a></li>
									</ul>
								</li>
								<li>When you are finished, click Done and the window will close.</li>
								<li>Click on Manage search engines... again (see Step 2 above), locate iZSearch under the list of Other search engines, place your mouse over the iZSearch entry in the list and click Make default.</li>
								<li>When you are finished, click Done and close the the Settings tab.</li>
							</ol>
							<p><b>Note:</b> If you cannot find the above options in your Chrome browser, please make sure that your Chrome version is up to date.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/what-is-ssl.html">What is a SSL/TLS Server Certificate?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>Whenever you use your web browser to communicate securely over the Internet (i.e. for banking, private search through iZSearch, etc.) you are using a technology known as SSL (Secure SocketLayer) or TLS (Transport Layer Security). This technology serves TWO important purposes:</p>
							<ol>
								<li>It provides end-to-end encryption of all data, so that your ISP, the NSA, a random hacker, etc. can't eavesdrop on the connection between you and the website you are communicating with (i.e. the bank, iZSearch, etc.).</li>
								<li>
									<p>It provides authentication so you can confirm that the website you are communicating with is actually who it claims to be. That lets you confirm that a thief is not masquerading as your bank, or that a spy is not masquerading as iZSearch.</p>
									<p>These two functions are both distinct and necessary. A connection to a trusted party is useless if it is not secure, and a secure connection to a party you can't trust is also useless.</p>
									<p>An SSL/TLS Server Certificate provides a website's authentication. You can think of it as being like a drivers license or a student ID card from a well-known university. It provides "official" identification for the website. If you need to prove your identity, you show your driver's license. If a website needs to do the same, it shows its SSL Server Certificate.</p>
									<p>Government ID might be issued by your state DMV, or a passport issued by the U.S. Federal government. "Internet ID" in the form of a SSL/TLS Server Certificate, is issued by a "Certificate Authority" (CA) -- a business entity entrusted with issuing SSL/TLS Server Certificates just like individual states are entrusted with issuing drivers' licenses. At last count, Mozilla's Firefox browser (for example) maintained a list of roughly 36 trusted CAs in total.</p>
									<p>If an SSL/TLS Server Certificate is not issued by one of these trusted CAs then your browser will typically either refuse the connection or issue numerous warnings you must override. That situation is like a bartender rejecting an ID card cooked up by someone's fraternity brother, also known as a "Fake ID."</p>
									<p>Wikipedia has comprehensive technical articles on these topics here: <a href="https://en.wikipedia.org/wiki/Certificate_authority">https://en.wikipedia.org/wiki/Certificate_authority</a><br><a href="https://en.wikipedia.org/wiki/Transport_Layer_Security">https://en.wikipedia.org/wiki/Transport_Layer_Security</a></p>
									<p>You can ask your web browser to show you the SSL/TLS Server Certificate for any secure website you visit.</p>
									<span>If you ask for iZSearch's (current as of 1/28/2015) SSL/TLS Server Certificate, you'll see:</span>
									<table>
										<thead>
											<tr>
												<td>Issued To</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Common Name (CN)</td>
												<td>*.iZSearch.com</td>
											</tr>
											<tr>
												<td>Organization (O)</td>
												<td>*.iZSearch.com</td>
											</tr>
											<tr>
												<td>Organizational Unit (OU)</td>
												<td>Domain Control Validated</td>
											</tr>
											<tr>
												<td>Serial Number</td>
												<td>27:9B:AA:D9:AD:ED:A9</td>
											</tr>
										</tbody>
									</table>
									<table>
										<thead>
											<tr>
												<td>Issued By</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Common Name (CN)</td>
												<td>Go Daddy Secure Certification Authority</td>
											</tr>
											<tr>
												<td>Organization (O)</td>
												<td>GoDaddy.com, Inc.</td>
											</tr>
											<tr>
												<td>Organizational Unit (OU)</td>
												<td>http://certificates.godaddy.com/repository</td>
											</tr>
										</tbody>
									</table>
									<table>
										<thead>
											<tr>
												<td>Validity Period</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Issued On</td>
												<td>3/16/11</td>
											</tr>
											<tr>
												<td>Expires On</td>
												<td>3/24/13</td>
											</tr>
										</tbody>
									</table>
									<table>
										<thead>
											<tr>
												<td>Fingerprints</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>SHA-256 Fingerprint</td>
												<td>34 78 92 BD 2C CF D3 46 81 C9 D0 24 72 E4 7A DA</td>
											</tr>
											<tr>
												<td>SHA-1 Fingerprint</td>
												<td>5C CF 20 7F 8C 8E 08 FC D8 D1 60 89 58 06 49 4C</td>
											</tr>
										</tbody>
									</table>
								</li>
							</ol>
							<p>This information tells you that iZSearch was issued an SSL/TLS Server Certificate by the "Go Daddy Secure Certification Authority." GoDaddy.com, Inc. is one of several different Certificate Authorities from whom a business or website such as iZSearch may purchase an SSL/TLS Server Certificate.</p>
							<p>Other major providers are GeoTrust, Comodo, Verisign, and Thawte. (See: <a href="http://www.whichssl.com/ssl-market-share.html">http://www.whichssl.com/ssl-market-share.html)</a></p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/getting-cookies.html">Am I still getting cookies even though I am using your service?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies of any kind.</p>
							<p>There are two ways you could pick up a cookie when using our service:</p>
							<p>The first is an anonymous iZSearch Settings cookie that you can choose to accept on our Settings page. This is a privacy-friendly cookie that remembers your preferences each time you return to iZSearch. It does not uniquely identify you and it is never used for tracking. (Note that if you choose not to install the Settings cookie, you can still save your settings with the URL generator at the bottom of the Settings page instead.)</p>
							<p>The second way you could be picking up cookies is from third-party websites you visit through iZSearch. This occurs when you leave iZSearch by clicking on a website result. Here's how it works:</p>
							<p>Whenever you search with iZSearch, your SEARCH is not recorded, your IP ADDRESS is not recorded, your IDENTITY is not recorded, no TRACKING COOKIES are placed on your browser, and our SSL/TLS ENCRYPTION ensures that your ISP or hackers can't eavesdrop. You are never seen by any of the search engines, including Google.</p>
							<p>That protection applies 100% to your SEARCHES. However, when you CLICK on a search result, you LEAVE the search engine that gave you the link and go SOMEWHERE ELSE.</p>
							<p>Wherever you go, once you leave <b>iZSearch</b>, you can be seen, recorded, and tracked by the website you are going to, plus by all of its advertising and marketing and tracking partners and affiliates.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/mylocation.html">How do you know my language or location if you don't store information about me?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not record IP addresses. We determine which browser you are using (Internet Explorer, Firefox, Safari, etc.) so we can customize the link on our home page that allows you to add iZSearch to your browser. Since your browser configuration also tells us, anonymously, what language your browser is set to, that allows us to seamlessly deliver content in the language you are using. In addition to the search term, we use an anonymized country code that is required to provide results at the country level for your search. In no circumstance do we share your actual IP address, exact location, or other personally identifiable data.</p>
							<p>We do not log or make any permanent record of which browser you are using or which language your browser is set to, since we keep no record of your presence at all.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/cookie-policy.html">What is iZSearch's cookie policy?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch does not use tracking cookies, and we do not access or review cookies placed on your browser by other websites.</p>
							<p>If you use the iZSearch Settings page and save your preferences, or if you change the language setting from our home page, a single anonymous cookie is used to save your search preferences. If you do not wish to receive that cookie, you can use the "URL Generator" feature on our Settings page instead.</p>
							<p>Two optional iZSearch features also use temporary session-only cookies. These cookies are not stored by iZSearch and they disappear at the close of the session:</p>
							<ul>
								<li>When you're on the classic.iZSearch.com site, iZSearch gives you the option of refining your search to exclude results you have already seen (The "Search within results" feature at the bottom of a search results page). If used, this sets a session-only cookie, which is not logged and which disappears at the end of the session. That cookie contains one-way-encrypted indicators of the previously displayed results to support this feature. The "lossy encryption" technique makes it impossible to determine which result was clicked on after the fact. That information is also not stored.</li>
								<li>Certain websites offer partner-specific versions of iZSearch to their users. In this case, if a user searches iZSearch from that website, that website's logo might appear in the iZSearch header. In those cases, a session-only cookie containing the partner code is set, allowing iZSearch to display that logo on subsequent searches in the same session. At the close of the session the cookie disappears.</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/server-logs.html">What exactly gets recorded in your server logs? (IP Address? User Agent?)</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>We have customized our Web server software so that it does not log any IP addresses or user agents. It writes 0.0.0.0 in lieu of an IP address and leaves a blank space for the user agent. No unique IP address is ever stored on our systems. More information can be found in our <a href="${baseURL}/private.html">Privacy Policy</a>.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/izsearch-architecture.html">What is Architecture of iZSearch?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is coded in Java, served via nginx, FastCGI and memcached, running on FreeBSD and Ubuntu via daemontools. We both run our own servers across the world. Your connection generally goes to the closest regional server available to your area. We use PostgreSQL, SOLR, and flat files for data. We monitor via Server Density, our own scripts and DNS Made Easy (which we also use for DNS and failover). Finally, we have some side components that use Prosody, Debian, ejabberd, jQuery, node.js, Python and even more Perl with lots of CPAN distributions.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/privacy-protection.html">Does iZSearch add privacy protection to other sites I visit?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>No, iZSearch does not add privacy protection to websites you visit. The websites you visit are third-party websites and are not part of iZSearch. iZSearch protects your privacy as you search for information. No one can see you searching or what you have searched in the past.</p>
							<p>However, once you find your result in iZSearch and click on it, you are no longer actively on our website and our privacy protections do not apply. By leaving iZSearch and going to a different website, you allow that website to see your IP address and place tracking cookies on your browser. They are able to determine your location and possibly gather additional personally identifiable information about you.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/windows-compatible.html">Is iZSearch compatible with Windows 10 / Edge?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch is now compatible with Windows 10/Edge.</p>
							<p>To add iZSearch as a default search engine in Edge, please open your Edge browser, and follow the instructions given here: <a href="https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1">https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1</a> To add iZSearch as Edge's homepage, you can follow these instructions:</p>
							<ol>
								<li>Click on the three dots (the <b>More actions</b> button) in the top right.</li>
								<li>Click on the <b>Settings</b> menu item.</li>
								<li>Locate the <b>Open</b> with section.</li>
								<li>Select the A <b>specific page or pages option</b>.</li>
								<li>Click on the dropdown menu below and choose Custom.</li>
								<li>Click the <b>X</b> cross beside any pages you do not want (ie. about:start).</li>
								<li>Enter: <a href="https://iZSearch.com">https://iZSearch.com</a> into the text-box, and click the plus sign + to add. (Please note, you can also decide to enter a settings URL in this field. To learn more about <a href="https://iZSearch.com/eng/download-iZSearch-plugin.html?&hmb=1">this</a> option, please visit this article from our Support Center).</li>
								<li>Restart Microsoft Edge to see iZSearch.com open.</li>
							</ol>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/collection-policy.html">What is your data collection policy? What do you record?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>We don't collect any personal information on our visitors.</p>
							<p>When you use iZSearch, we do not record your IP address, we do not record which browser you are using (Internet Explorer, Safari, Firefox, etc.), we do not record your computer platform (Windows, Mac, Linux, etc.), and we do not record the words or phrases you searched for.</p>
							<p>The only information we record is an aggregate total of how many searches are performed on our website each day (a measure of overall traffic), and we break down those overall traffic numbers by language.</p>
							<p>Our zero data-collection policy is important, because even seemingly harmless information can be combined to reveal more information than you might care to disclose.</p>
							<p>As a result, we never collect or store your IP address, browser, or platform information, because that data can be used to uniquely identify your computer or location -- or it can be combined with other data to learn even more about you. Your search terms can also convey personal information (think of someone entering a name together with a social security number) so we never collect or record those either.</p>
							<p>Please click this link to view our Privacy Policy.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/favorite-websites.html">Can iZSearch store my favorite websites and bookmarks?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>iZSearch makes a point of not storing your searches and not recording the websites you visit.</p>
							<p>However iZSearch has a big library of popular resources that you can directly serach on (feature similar to the sites like Pinterest and Linklist).</p>
							<p>Another (actually a standard) way to store and remember your favorite websites is through your web browser (Internet Explorer, Firefox, Safari, Chrome, Opera, etc.) The feature you want is usually called "Favorites" or "Bookmarks." You can review your browser documentation for more details on how to use this feature.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/extended-validation.html">Does iZSearch have an Extended Validation (EV) SSL certificate?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>Traditional SSL certificates provide basic information about the certificate holder, as well as the organization that issued the certificate. However, the amount of validation done to confirm the identify of the organization requesting the certificate varies considerably. To obtain an EV SSL certificate, the certificate holder must first go through a rigorous validation process to confirm itself as a legally-established business or organization with a verifiable identity. We are researching the best approach to replace our current www.izsearch.com certificates. To learn more about EV SSL standards and SSL technology, please visit <a href="https://en.wikipedia.org/wiki/Extended_Validation_Certificate.">https://en.wikipedia.org/wiki/Extended_Validation_Certificate.</a></p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/encryption-protect.html">How does HTTPS encryption protect me? Does it keep my ISP from seeing me?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>The "https (SSL or "secure socket layer"/TLS "transport layer security") encrypted" connection you make with iZSearch prevents your Internet service provider (ISP) from seeing what you're searching for while you are on iZSearch.</p>
							<p>For maximum privacy, here are a few tips: You should understand that whenever you search with iZSearch, your SEARCH is not recorded, your IP ADDRESS is not recorded, your IDENTITY is not recorded, no TRACKING COOKIES are placed on your browser, and our "HTTPS (SSL/TLS) ENCRYPTION" ensures that your ISP or hackers can't eavesdrop on what you search for or see through iZSearch. You are also never seen by any of the search engines (e.g. Google).</p>
							<p>That protection applies 100% to your SEARCHES. However, when you CLICK on a search result, including sponsored results, you LEAVE the search engine that gave you the link and go SOMEWHERE ELSE.</p>
							<p>Once you leave iZSearch, you can be seen, recorded, and tracked by the website you are going to, plus by all of its advertising and marketing and tracking partners and affiliates.</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4><a href="${baseURL}/subpages/background-information.html">Where can I find more background information on privacy issues?</a></h4>
						</div>
						<div class="qbody issue-body">
							<p>The websites of various privacy organizations are a great starting point for locating an abundance of background information on this subject. iZSearch recommends:
								<ul>
									<li><a href="http://privacyrights.org/">http://privacyrights.org/</a></li>
									<li><a href="http://www.eff.org/">http://www.eff.org/</a></li>
									<li><a href="http://epic.org">http://epic.org</a></li>
									<li><a href="http://worldprivacyforum.org/">http://worldprivacyforum.org/</a></li>
								</ul>
							</p>
						</div>
					</div>
				</li>
				<li class="list-group-item" data-bs="0">
					<div class="group-img">
						<i class="fa fa-question-circle"></i>
					</div>
					<div class="qa issue-group">
						<div class="qheader issue-title">
							<h4>Where is the site map?</h4>
						</div>
						<div class="qbody issue-body">
							<p>You can find sitemap by following this link: <a href="${baseURL}/subpages/sitemap.html">iZSearch sitemap</a></p>
						</div>
					</div>
					<div class="btn-ec" title="Click to expand/collapse text body.">
						<i class="fa fa-angle-double-down"></i>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
