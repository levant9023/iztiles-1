<%--
    Document   : less_ads
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>


<c:set var='title' value='less ads' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <a href="/"><img src="${baseURL}/resources/img/logo_search.svg" /></a>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-3 text-left"></div>
      <div class="col-md-6 text-center">
        <h2>Confidency</h2>
        <p>Some title</p>
      </div>
      <div class="col-md-3 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-2"></div>
    <div id="lsa-text" class="col-md-8">
      <ul class="arts list-unstyled">
        <li>
          <h3>Confidency</h3>
          <p>Some Text</p>
        </li>
      </ul>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>