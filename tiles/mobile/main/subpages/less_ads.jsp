<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<c:set var="title" value="less ads" scope="request"/>

<div class="container-fluid">
	<div id="subpage-board" class="row">
		<div id="subpage-logo" class="row">
			<div class="col-md-4 text-left"></div>
			<div class="col-md-4 text-center">
				<a href="/"><img src="resources/img/logo_search.svg" /></a>
			</div>
			<div class="col-md-4 text-right"></div>
		</div>
		<div id="subpage-motto" class="row">
			<div class="col-md-3 text-left"></div>
			<div class="col-md-6 text-center">
				<h2>More privacy. Less ads.</h2>
				<p>iZSearch offers a whole new experience to Search, Browse and Discover the Web.</p>
			</div>
			<div class="col-md-3 text-right"></div>
		</div>
	</div>
	<div id="subpage-arts" class="row">
		<div class="col-md-2"></div>
		<div id="subpage-lsa-text" class="col-md-8">
			<ul class="arts list-unstyled">
				<li>
					<p>iZSearch is a general purpose search engine that finds and returns relevant web sites, images, videos and realtime results.</p>
					<p>We offer a search service which does not retain or share any of your personal information. iZSearch does not download “cookies” onto people’s devices. It does not register the “IP address” which pinpoints a users computer. It does not "filter" search results. That is distinct from what other companies do. We are not anonymizing/encrypting the data. We actually throw it away - everything related to the user and anything that is personally identifiable.</p>
					<p>By default, iZSearch shows only <b>minimal ads</b> at the bottom of the search results page. iZSearch <b>does not sell data about you</b> to third parties, including advertisers and data brokers.</p>
					<p>Search It Easy with iZSearch!</p>
				</li>
			</ul>
		</div>
	</div>
</div>
