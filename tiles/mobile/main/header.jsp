<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<c:set var="meta_title" value="${requestScope.meta_title}" />

<base href="${baseURL}/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>iZSearch search engine | <c:out value="${meta_title} Mobile version." default="iZSearch search engine. Mobile version."/> </title>
<meta name="description" content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results.">
<meta name="keywords" content="izsearch, izsearch home, izsearch home page, izsearch search, izsearch web, news, blog, forum, health, sports, travel, tech, entertainment, video, image">

<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/mobile/main.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/plugins/social-share/jquery.share.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
<link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/can.custom.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/plugins/social-share/jquery.share.js"></script>

<script type="text/javascript" data-main="${baseURL}/resources/js/production/mobile/js/main_app.js" src="${baseURL}/resources/js/production/mobile/js/require.js"></script>
