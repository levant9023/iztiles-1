<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 7/27/15
  Time: 11:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<base href="${baseURL}/">
<meta http-equiv="X-UA-Compatible" content="IE=edge; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
      content="izSearch search engine, the new private way to search the web. Search engine that finds and returns relevant web sites, images, videos and realtime results. Search easy with izSearch!">
<meta name="keywords"
      content="search engine, web search, privacy, private search, image search, video search, search engine privacy, search engine optimization, search engine marketing, easy search">
<title>iZSearch:&nbsp;<c:out value="${meta_title}" default=""/></title>
<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png"/>
<link href="https://izsearch.com/resources/img/favicon.png" rel="shortcut icon">
<link rel="icon" type="image/png" href="https://izsearch.com/resources/img/favicon.png">
<link rel="search" type="application/opensearchdescription+xml" title="iZSearch" href="${baseURL}/provider.xml">
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap-datetimepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/css/owl.carousel.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/css/owl.theme.css"/>
<link rel="stylesheet" type="text/css" href="${baseURL}/resources/css/mobile/search.css"/>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/moment-locales.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/vendors/can.custom.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/classes.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/icons.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/modules/js.cookie.js"></script>
<script type="text/javascript" src="${baseURL}/resources/js/production/mobile/js/mvc/controls/ads/adv.js"></script>
<script type="text/javascript">
  var query = "<c:out value='${query}' default=''/>";
  var lang = "<c:out value='${lang}' default='en'/>";
  var geo = {};

</script>
<script type="text/javascript"
        data-main="${baseURL}/resources/js/production/mobile/js/mvc/search_app.js"
        src="${baseURL}/resources/js/production/mobile/js/require.js">
</script>
<script type="text/javascript">
  var request = new XMLHttpRequest();
  request.open("GET", "/ads/geo.html"+window.location.search, false);
  request.send(null);
  try {
    if (request.readyState === 4 && request.status === 200) {
      geo = JSON.parse(request.responseText);
    }
  }catch (e){
    console.log(e);
  }
</script>
