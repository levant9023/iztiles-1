<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="content">
	<tiles:insertAttribute name="wikiblock"/>
	<tiles:insertAttribute name="top1"/>
	<div id="amazon_ads" class="hide_amazon">
		<div id="adunit0">
			<script type="text/javascript">
				var query = '${requestScope.q}';
				amzn_assoc_placement = 'adunit0';
				amzn_assoc_search_bar = 'false';
				amzn_assoc_tracking_id = 'izsearchcom0c-20';
				amzn_assoc_search_bar_position = 'bottom';
				amzn_assoc_ad_mode = 'search';
				amzn_assoc_ad_type = 'smart';
				amzn_assoc_marketplace = 'amazon';
				amzn_assoc_region = 'US';
				amzn_assoc_title = '';
				amzn_assoc_default_search_phrase = query;
				amzn_assoc_default_category = 'All';
				amzn_assoc_linkid = '5c74723557905a17a40bd758b812bbe3';
				amzn_assoc_rows = '1';
			</script>
			<script src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US"></script>
		</div>
	</div>
	<div id="adv-ad" class="adv"><div class="ad-wrapper _ak"></div></div>
	<%--tiles:insertAttribute name="newsblock"/--%>
	<div id="newsblock" class="block"></div>

	<%--tiles:insertAttribute name="videoblock"/--%>
	<div id="videoblock" class="block"></div>

	<tiles:insertAttribute name="top2"/>

	<!-- div id="ali-place"></div -->
	<tiles:insertAttribute name="favesblock"/>
	<div id="amazon_unic2"></div>
	<tiles:insertAttribute name="similar-search"/>
	<tiles:insertAttribute name="paginator"/>
</div>
<!-- a id="top" href="javascript://">Top</a -->
