<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<%
	Date now = new Date();
	Calendar cal = Calendar.getInstance();
	cal.setTime(now);

	cal.add(Calendar.DAY_OF_MONTH, -1);
	Date past_day = cal.getTime();

	cal.add(Calendar.DAY_OF_MONTH, -7);
	Date past_week = cal.getTime();

	cal.add(Calendar.MONTH, -1);
	Date past_month = cal.getTime();

	cal.add(Calendar.YEAR, -1);
	Date past_year = cal.getTime();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	pageContext.setAttribute("now", sdf.format(now));
	pageContext.setAttribute("past_day", sdf.format(past_day));
	pageContext.setAttribute("past_week", sdf.format(past_week));
	pageContext.setAttribute("past_month", sdf.format(past_month));
	pageContext.setAttribute("past_year", sdf.format(past_year));
%>

<c:set var="query" value="${empty requestScope.q ? '' : requestScope.q}" scope="request"/>
<c:set var="type" value="${empty requestScope.t ? '' : requestScope.t}" scope="request"/>
<c:set var="offset" value="${empty requestScope.offset ? '0' : requestScope.offset}" scope="request"/>
<c:set var="records" value="${empty requestScope.records ? '10' : requestScope.records}" scope="request"/>
<c:set var="date_start" value="${empty requestScope.start ? '' : requestScope.start}" scope="request"/>
<c:set var="date_stop" value="${empty requestScope.stop ? '' : requestScope.stop}" scope="request"/>
<c:set var="sort" value="${requestScope.sort}" scope="request"/>
<c:set var="approximativeResults" value="${requestScope.approximativeResults}" scope="request"/>
<c:set var="maininfo" value="${requestScope.infotest}" scope="request"/>
<c:set var="sortTitle" value="Sort by date"/>

<c:choose>
	<c:when test="${logo == 'forums'}">
		<c:set var="logo" value="forums"/>
	</c:when>
	<c:otherwise>
		<c:set var="logo" value="${iz:basename(requestScope['javax.servlet.forward.request_uri'])}"/>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${empty sort}">
		<c:set var="sort" value="date:desc"/>
		<c:set var="sortTitle" value="Sort by date"/>
	</c:when>
	<c:otherwise>
		<c:if test="${sort == 'date:desc'}">
			<c:set var="sort" value=""/>
			<c:set var="sortTitle" value="Sort by relevance"/>
		</c:if>
	</c:otherwise>
</c:choose>

<c:set var="requestURL" value="${baseURL}${requestScope.wpath}" scope="request"/>

<c:url var="rootUrl" value="${baseURL}"><c:param name="q" value="${query}"/></c:url>

<c:url var="sortUrl" value="${baseURL}">
	<c:param name="q" value="${query}"/>
	<c:param name="start" value="${date_start}"/>
	<c:param name="stop" value="${date_stop}"/>
	<c:param name="sort" value="${sort}"/>
</c:url>

<c:set var="count" value="10" scope="page"/>

<c:url var="bagreport_url" value="/bugreport.html?page=1"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="main_url" value="${baseURL}"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="images_url" value="/images.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="videos_url" value="/videos.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="map_url" value="/map.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="news_url" value="/news.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="forums_url" value="/forum.html"><c:param name="q" value="${param.q}"/></c:url>

<c:url var="art_url" value="/art.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="blogs_magazines_url" value="/blogs_magazines.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="books_url" value="/books.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="business_url" value="/business.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="cars_url" value="/cars.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="celebrity_url" value="/celebrity.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="craft_url" value="/craft.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="design_url" value="/design.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="education_url" value="/education.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="family_url" value="/family.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="food_recipes_url" value="/food_recipes.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="fun_url" value="/fun.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="gifts_url" value="/gifts.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="health_url" value="/health.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="home_url" value="/home.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="jobs_url" value="/jobs.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="industrial_url" value="/industrial.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="kids_url" value="/kids.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="law_url" value="/law.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="lifestyle_url" value="/lifestyle.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="men_url" value="/men.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="movies_url" value="/movies.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="music_url" value="/music.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="npo_url" value="/npo.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="pets_url" value="/pets.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="politics_url" value="/politics.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="real_estate_url" value="/real_estate.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="science_url" value="/science.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="seniors_url" value="/seniors.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="shopping_url" value="/shopping.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="sports_url" value="/sports.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="style_fashion_url" value="/style_fashion.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="teachers_url" value="/teachers.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tech_url" value="/tech.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="teens_url" value="/teens.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tips_tutorials_url" value="/tips_tutorials.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="travel_url" value="/travel.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tools_url" value="/tools.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="tv_url" value="/tv.html"><c:param name="q" value="${param.q}"/></c:url>
<c:url var="women_url" value="/women.html"><c:param name="q" value="${param.q}"/></c:url>

<div id="header">
	<!--<ul class="submenu">
		<li class="pm hide"><a href="javascript://" data-object="#rsads-wrapper"><i class="glyphicon glyphicon-envelope"></i></a></li>
		<li class="pm hide"><a href="/bugreport.html?page=1"><i class="glyphicon glyphicon-list-alt"></i></a></li>
		<li><a class="show-more" href="javascript://"><i class="glyphicon glyphicon-menu-hamburger"></i></a></li>
	</ul>-->
	<div id="head-wraper">
		<div id="logo">
			<a href="/">
				<img src="${baseURL}/resources/img/logos/logo_search.svg" alt="Logo">
			</a>
		</div>
		<form id="search-form" action="${requestScope['javax.servlet.forward.servlet_path']}" method="GET" target="_self">
			<input id="input-search" type="text" autocomplete="off" name="q" value="${query}"/>
			<span id="clear" style="display: none;">×</span>
			<a class="search-btn glyphicon glyphicon-search" href="javascript://"></a>
		</form>
	</div>
	<span class="cat-select"><a class="show-hide-sub-menu" href="javascript://"><%--<img src="${baseURL}/resources/img/nav/menu.png" class=".icon-inert">--%>MORE</a></span>
	<div id="bloc_menu">
		<div id="cts-menu">
			<ul class="active-menu">
				<li class="the-all"><a href="${main_url}">All</a></li>
				<li class="the-news"><a href="${news_url}">News</a></li>
				<li class="the-images"><a href="${images_url}">Images</a></li>
				<li class="the-video"><a href="${videos_url}">Videos</a></li>
				<li class="the-blogs_magazines"><a href="${blogs_magazines_url}">Blogs</a></li>
				<!--Test-->
				<li class="the-art"><a href="${art_url}"><span>Art</span></a></li>
				<li class="the-books"><a href="${books_url}"><span>Books</span></a></li>
				<li class="the-business"><a href="${business_url}"><span>Business</span></a></li>
				<li class="the-cars"><a href="${cars_url}"><span>Cars</span></a></li>
				<li class="the-celebrity"><a href="${celebrity_url}"><span>Celebrity</span></a></li>
				<li class="the-craft"><a href="${craft_url}"><span>Craft</span></a></li>
				<li class="the-design"><a href="${design_url}"><span>Design</span></a></li>
				<li class="the-education"><a href="${education_url}"><span>Education</span></a></li>
				<li class="the-family"><a href="${family_url}"><span>Family</span></a></li>
				<li class="the-food_recipes"><a href="${food_recipes_url}"><span>Food &amp; Recipes</span></a></li>
				<li class="the-fun"><a href="${fun_url}"><span>Fun</span></a></li>
				<li class="the-gifts"><a href="${gifts_url}"><span>Gifts</span></a></li>
				<li class="the-health"><a href="${health_url}"><span>Health</span></a></li>
				<li class="the-home"><a href="${home_url}"><span>Home &amp; Garden</span></a></li>
				<li class="the-jobs"><a href="${jobs_url}"><span>Jobs</span></a></li>
				<li class="the-industrial"><a href="${industrial_url}"><span>Industrial</span></a></li>
				<li class="the-kids"><a href="${kids_url}"><span>Kids</span></a></li>
				<li class="the-law"><a href="${law_url}"><span>Law</span></a></li>
				<li class="the-lifestyle"><a href="${lifestyle_url}"><span>Lifestyle</span></a></li>
				<li class="the-men"><a href="${men_url}"><span>Men</span></a></li>
				<li class="the-movies"><a href="${movies_url}"><span>Movies</span></a></li>
				<li class="the-music"><a href="${music_url}"><span>Music</span></a></li>
				<li class="the-npo"><a href="${npo_url}"><span>NPO</span></a></li>
				<li class="the-pets"><a href="${pets_url}"><span>Pets</span></a></li>
				<li class="the-politics"><a href="${politics_url}"><span>Politics</span></a></li>
				<li class="the-real_estate"><a href="${real_estate_url}"><span>Real Estate</span></a></li>
				<li class="the-science"><a href="${science_url}"><span>Science</span></a></li>
				<li class="the-seniors"><a href="${seniors_url}"><span>Seniors</span></a></li>
				<li class="the-shopping"><a href="${shopping_url}"><span>Shopping</span></a></li>
				<li class="the-sports"><a href="${sports_url}"><span>Sports</span></a></li>
				<li class="the-style_fashion"><a href="${style_fashion_url}"><span>Style &amp; Fashion</span></a></li>
				<li class="the-teachers"><a href="${teachers_url}"><span>Teachers</span></a></li>
				<li class="the-tech"><a href="${tech_url}"><span>Technology</span></a></li>
				<li class="the-teens"><a href="${teens_url}"><span>Teens</span></a></li>
				<li class="the-tips_tutorials"><a href="${tips_tutorials_url}"><span>Tips &amp; Tutorials</span></a></li>
				<li class="the-travel"><a href="${travel_url}"><span>Travel</span></a></li>
				<li class="the-tools"><a href="${tools_url}"><span>Tools</span></a></li>
				<li class="the-tv"><a href="${tv_url}"><span>TV</span></a></li>
				<li class="the-women"><a href="${women_url}"><span>Women</span></a></li>
			</ul>
		</div>
	</div>
	<ul class="sub-menu hide">
		<li class="the-art"><a href="${art_url}"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
		<li class="the-books"><a href="${books_url}"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
		<li class="the-blogs_magazines"><a href="${blogs_magazines_url}"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs &amp; Magazines</span></a></li>
		<li class="the-business"><a href="${business_url}"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
		<li class="the-cars"><a href="${cars_url}"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
		<li class="the-celebrity"><a href="${celebrity_url}"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
		<li class="the-craft"><a href="${craft_url}"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
		<li class="the-design"><a href="${design_url}"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
		<li class="the-education"><a href="${education_url}"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
		<li class="the-family"><a href="${family_url}"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
		<li class="the-food_recipes"><a href="${food_recipes_url}"><img src="/resources/img/nav/food_recipes.svg"><span>Food &amp; Recipes</span></a></li>
		<li class="the-fun"><a href="${fun_url}"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
		<li class="the-gifts"><a href="${gifts_url}"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
		<li class="the-health"><a href="${health_url}"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
		<li class="the-home"><a href="${home_url}"><img src="/resources/img/nav/home.svg"><span>Home &amp; Garden</span></a></li>
		<li class="the-jobs"><a href="${jobs_url}"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
		<li class="the-industrial"><a href="${industrial_url}"><img src="/resources/img/nav/industrial.svg"><span>Industrial</span></a></li>
		<li class="the-kids"><a href="${kids_url}"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
		<li class="the-law"><a href="${law_url}"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
		<li class="the-lifestyle"><a href="${lifestyle_url}"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
		<li class="the-men"><a href="${men_url}"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
		<li class="the-movies"><a href="${movies_url}"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
		<li class="the-music"><a href="${music_url}"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
		<li class="the-news"><a href="${news_url}"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
		<li class="the-npo"><a href="${npo_url}"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
		<li class="the-pets"><a href="${pets_url}"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
		<li class="the-politics"><a href="${politics_url}"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
		<li class="the-real_estate"><a href="${real_estate_url}"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
		<li class="the-science"><a href="${science_url}"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
		<li class="the-seniors"><a href="${seniors_url}"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
		<li class="the-shopping"><a href="${shopping_url}"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
		<li class="the-sports"><a href="${sports_url}"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
		<li class="the-style_fashion"><a href="${style_fashion_url}"><img src="/resources/img/nav/style_fashion.svg"><span>Style &amp; Fashion</span></a></li>
		<li class="the-teachers"><a href="${teachers_url}"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
		<li class="the-tech"><a href="${tech_url}"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
		<li class="the-teens"><a href="${teens_url}"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
		<li class="the-tips_tutorials"><a href="${tips_tutorials_url}"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips &amp; Tutorials</span></a></li>
		<li class="the-travel"><a href="${travel_url}"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
		<li class="the-tools"><a href="${tools_url}"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
		<li class="the-tv"><a href="${tv_url}"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
		<li class="the-women"><a href="${women_url}"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
	</ul>
</div>
<div id="autosuggest">
	<ul id="autocomplete" class="autocomplete"></ul>
</div>
<script>
	$(document).ready(function(){
		//Кнопка удаления содержимого строки запроса
		var cont = $('#input-search').val();
		if(cont.length != 0 ) {
			$('#clear').removeAttr('style');
		}else{
			$('#clear').css('display','none');
		}
		$('#input-search').keyup(function() {
			var cont = $('#input-search').val();
			if(cont.length != 0 ) {
				$('#clear').removeAttr('style');
			}else{
				$('#clear').css('display','none');
			}
		});
		$("#clear").click(function(){
			$('#input-search').val('');
			$('#clear').css('display','none');
		})

	});
</script>