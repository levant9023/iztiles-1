
<!-- Header -->
<header id="header">
  <div class="container col-xs-12 col-lg-6">

    <div id="logo" class="pull-left">
      <a href="${baseURL}/"><img src="${baseURL}/resources/img/brands/izbrands_squared_logo.png" alt="" title=""/></a>
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu">
        <li><a href="${baseURL}/brand/intro.html">Home</a></li>
        <li><a href="${baseURL}/brand/method.html">Method</a></li>
        <li><a href="${baseURL}/brand/contact.html">Contact</a></li>
        <li><a href="http://izsearch.com">izsearch.com</a></li>
      </ul>
    </nav>
    <!-- #nav-menu-container -->

    <nav class="nav social-nav pull-right d-none d-lg-inline">
      <a href="https://www.twitter.com/izsearch"><i class="fa fa-twitter"></i></a> <a href="https://www.facebook.com/izsearchcom"><i class="fa fa-facebook"></i></a>
      <a href="https://www.linkedin.com/company/izsearch"><i class="fa fa-linkedin"></i></a>
      <a href="mailto:contact@izsearch.com"><i class="fa fa-envelope"></i></a>
    </nav>
  </div>
</header>
<!-- #header -->