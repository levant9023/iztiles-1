<div class="wrapper col-xs-12 col-lg-6">
  <section id="contact">
    <div class="container frm">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="section-title">Get In Touch</h2>
          <div class="col-md-12 col-lg-12">
            <div class="contact-descr"><p>
              Simply fill out the form and one of our experts will be in touch to show you the Demo.
              Learn and discuss what iZ Brands can tell you about your customers and how to increase your sales.
            </p></div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-9 col-md-8">
          <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form id="contact-form" action="" method="post" role="form" class="contactForm">
              <div class="form-group">
                <input type="text" name="name" class="form-control" id="name" placeholder="Full Name"
                       data-rule="minlen:4" title="Please enter at least 4 chars">
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Work email"
                       data-rule="email" title="Please enter a valid email">
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="company" id="company" placeholder="Company/Organization"
                       data-rule="minlen:4" title="Please enter at least 3 chars of Comany/Organization">
                <div class="validation"></div>
              </div>
              <div class="form-group">
              <input type="text" class="form-control" name="title"  id="title" placeholder="Job Title"
                     data-rule="minlen:4" title="Please enter at least 5 chars of job title">
              <div class="validation"></div>
            </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required"
                          title="Please write something for us" placeholder="Describe briefly your case"></textarea>
                <div class="validation"></div>
              </div>
              <input type="hidden" name="_vl_" value="j345ix348!3hje">
              <div class="text-center">
                <span class="load-img"><img src="${baseURL}/resources/img/load.gif"></span>
                <button type="button">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>