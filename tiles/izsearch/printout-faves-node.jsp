<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:forEach var="children" items="${node.children}">
  <c:set var="node" value="${children.value}" scope="request"/>
  <c:if test="${fn:length(node.value) > 0}">
    <ul class="faves-list">
      <li class="li-title"><h5>${node.category}</h5></li>
      <c:forEach var="fave" items="${node.value}" varStatus="fave_statuts">
        <li class="liel grad">
          <div class="fave_wrap">
            <a href="${fave.directUrl}" target="_blank" title="${fave.description}">
              <c:set var="img" value="/images/faves/${fave.id}_${fave.title}.png" />
              <img src="${img}" alt="image"/>
            </a>
          </div>
        </li>
      </c:forEach>
    </ul>
  </c:if>
  <jsp:include page="printout-faves-node.jsp"/>
</c:forEach>