<%--  
  User: eFanchik
  Date: 07.07.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div id="sideCenter" class="center_container col-xs-10 col-sm-10">
  <div id="search-text" class="content">
    <tiles:insertAttribute name="sections"/>
  </div>
  <tiles:insertAttribute name="paginator"/>
</div>