<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 29.01.15
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<c:set var="news" value="${requestScope.lastVideos}" />
<c:forEach var="section" items="${news}" varStatus="sec_status">
  <c:set var="articles" value="${section.allArticles}" />
  <section class="sec">
    <c:set var="hidden" value="" />
    <c:forEach var="article" items="${articles}" varStatus="art_status" >
      <c:set var="source" value="${article.source}"/>
      <c:set var="pubdate" value="${article.pubDate}"/>
      <c:set var="title" value="${iz:text_normalize(article.title)}"/>
      <c:set var="title_cuted" value="${title}"/>
      <c:set var="descr" value="${iz:text_normalize(article.cleanedText)}"/>
      <c:set var="descr_cuted" value='${descr}'/>
      <c:if test="${fn:length(title) > 50}">
        <c:set var="title_cuted" value='${iz:trunc_words(title," ", 50)}&hellip;'/>
      </c:if>
      <c:if test="${fn:length(descr) > 120}">
        <c:set var="descr_cuted" value='${iz:trunc_words(descr," ", 120)}&hellip;'/>
      </c:if>
      <c:set var="url" value="${article.link}"/>
      <c:set var="link" value='${iz:top_private_domain(url)}'/>
      <c:if test="${art_status.index > 0}">
        <c:set var="hidden" value="hidden"/>
      </c:if>
      <article class="art ${hidden}" data-count="${art_status.index}">
        <c:if test="${iz:is_img(article.imgUrls[0]) == true}">
          <c:out value='<div class="img"><img src="${article.imgUrls[0]}"/></div>' escapeXml="false"/>
        </c:if>
        <div class="data">
          <h3 class="art-title"><a href="${url}">${title_cuted}</a></h3>
          <div class="art-sublink">
            <span class="pub-date"><fmt:formatDate type="date" value="${pubdate}" /></span>
            <span class="delim">|</span>
            <span class="pub-link">${link}</span>
          </div>
          <div class="art-descr">${descr_cuted}</div>
          <div class="art-footer"></div>
        </div>
      </article>
    </c:forEach>
  </section>
</c:forEach>


<%--
<section class="art">
  <c:forEach var="item" items="${news}" varStatus="item_status">
    <c:set var="source" value="${item.source}"/>
    <c:set var="pubdate" value="${item.pubDate}"/>
    <c:set var="title" value="${iz:text_normalize(item.title)}"/>
    <c:set var="title_cuted" value="${title}"/>
    <c:set var="descr" value="${iz:text_normalize(item.cleanedText)}"/>
    <c:set var="descr_cuted" value='${descr}'/>
    <c:if test="${fn:length(title) > 50}">
      <c:set var="title_cuted" value='${iz:trunc_words(title," ", 50)}&hellip;'/>
    </c:if>
    <c:if test="${fn:length(descr) > 130}">
      <c:set var="descr_cuted" value='${iz:trunc_words(descr," ", 130)}&hellip;'/>
    </c:if>
    <c:set var="url" value="${item.link}"/>
    <c:set var="link" value='${fn:replace(iz:get_domain(url),"www.", "")}'/>
    <article class="art">
      <c:if test="${iz:is_img(item.imgUrls[0]) == true}">
        <c:out value='<div class="img"><img src="${item.imgUrls[0]}"/></div>' escapeXml="false"/>
      </c:if>
      <div class="data">
        <h3 class="art-title"><a href="${url}">${title_cuted}</a></h3>
        <div class="art-sublink">
          <span class="pub-date"><fmt:formatDate type="date" value="${pubdate}" /></span>
          <span class="delim">|</span>
          <span class="pub-link">${link}</span>
        </div>
        <div class="art-descr">${descr_cuted}</div>
        <div class="art-footer"></div>
      </div>
    </article>
  </c:forEach>
</section>
--%>