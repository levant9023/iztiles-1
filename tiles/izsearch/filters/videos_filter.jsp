<%-- 
    Document   : videos_filter
    Created on : Nov 11, 2014, 3:26:47 AM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/filters.css" />

<%@page import="com.biosearch.index.client.TextIdWithRank"%>
<%@page import="org.izsearch.beans.TitleInfo"%>
<%@page import="org.izsearch.helper.HtmlEntities"%>
<%@page import="org.izsearch.helper.HtmlStrUtils"%>
<%@page import="org.izsearch.helper.StringUtils"%>
<%@page import="org.izsearch.model.LuceneResponse"%>
<%@page import="org.izsearch.model.SimilarLink"%>
<%@page import="org.izsearch.tags.Functions"%>
<%@page import="org.izsearch.model.calculation.TextPart"%>
<%@page import="java.net.URL"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%
  int offset = (Integer) request.getAttribute("offset");
  int records = (Integer) request.getAttribute("records");
  String query = (String) request.getAttribute("q");
  String sort = (String) request.getAttribute("sort");
  String timeString = (String) request.getAttribute("timeString");
  LuceneResponse lr = (LuceneResponse) request.getAttribute("lines");
  List<List<SimilarLink>> sections = null;
  if (lr != null) {
      sections = (lr).getLines();
  }
%>
<script type="text/javascript">
    var offset = "<%=offset%>";
    var records = "<%=records%>";    
</script>

<%
    if (sections != null) {
        int sectionCount = 0;

        // Цикл по каждой секции для получения списка артиклей.
        for (List<SimilarLink> articles : sections) {

            //Collections.sort(article, new  ArticleBooleanCompare());

            // Счетчик артиклей с полем isShowAsSubsnippet установленным в true.
            int articleFilteredCount = 0;
            for (SimilarLink article : articles) {
                if (article.isShowAsSubsnippet()) {
                    articleFilteredCount++;
                }
            }

            // получаем имя HOST.
            String host = new URL(articles.get(0).getFirstLink().getUrl()).getHost();

            boolean showFloatImg = !articles.isEmpty();
            if (showFloatImg) {
                if (articles.size() == 1) {
                    showFloatImg = false;
                } else {
                    showFloatImg = !articles.get(articles.size() - 1).isShowAsSubsnippet();
                }
            }

%>
<!-- Рисуем секцию -->
<section class="section" data-state="false" data-name="sec<%=sectionCount%>" data-fcount="<%=articleFilteredCount%>"> 
    <%if (showFloatImg) {%><span class="float_img" style="display: none;"></span>
    <%        }
        // флаг отобразить/скрыть артикль
        String HideOrShow = "";

        // флаг отобразить артикль список/столбец.
        String ListOrGrid = "article";

        // 
        String articleTitle = "artTitle";

        // Стиль для титров внизу секции
        String SecFooterLink = "secFooter";
        if ((offset == 0) && (articleFilteredCount > 0)) {
            SecFooterLink = "secFooterOffset";
        }

        // Получаем имя основного домена.
        String domenUrl = "",
                domain = HtmlStrUtils.getDomainFromUrl(host);

        //
        String analitic = "Evaluate credibility and popularity of " + domain + " from Analytics.";

        // Вспомогательные классы для построения титров.
        List<TitleInfo> listOfTitles = new ArrayList<TitleInfo>();
        List<TitleInfo> listOfDomains = new ArrayList<TitleInfo>();
        List<String> ttl = new ArrayList<String>();
        TitleInfo tti, doml;
        
        


        for (int articleCount = 0; articleCount < articles.size(); articleCount++) {
            SimilarLink article = articles.get(articleCount);
            TextIdWithRank t = article.getFirstLink();
            List<TextPart> tps = article.getFirstLinkTextParts();
            String textPartTitles = "";
            if (tps != null) {
                for (TextPart tp : tps) {
                    if (!textPartTitles.isEmpty()) {
                        textPartTitles = textPartTitles + ", ";
                    }
                    textPartTitles = textPartTitles + "&rdquo;" + tp.getTitle() + "&ldquo;";
                }
            }

            // Получаем URL.
            String url = t.getUrl();
            String a_nod = "";
            String sublink = "";
            String subtitle = "";
            String fsubtitle = "";
            String descr = "";
            String descr2 = "";
            String desc_max = "";
            String desc_hint = "";
            String desc_min = "";

            double datelen = 0;

            String date = t.getDate();
            if (date != null && date.length() > 0) {
                date = date.substring(0, 4) + "." + date.substring(4, 6) + "." + date.substring(6, 8);
                date = date + '\u0020';
                datelen = StringUtils.getStrSizePx(date, "Arial", 14);

                if (sort == null || sort.isEmpty()) {
                    if (StringUtils.moreThatMonth(date)) {
                        date = "";
                    }
                }
            }

            // Если длина URL больше 65 обрезать и добавить символ "/".
            if (url.length() > 58) {
                url = Functions.trunc_str(url, 58);
            }
   
            // Удалить схему в начале url.
            url = url.replaceFirst("^http:\\/\\/", "");
            url = url.replaceAll("\\/$", "");

            // Устанавливаем титул с капитализированым 1 символом.
            if (!StringUtils.isEmpty(t.getTitle())) {
                subtitle = StringUtils.clearify(StringUtils.ucwords(t.getTitle(), "\u0020"));
                subtitle = HtmlEntities.htmlentities(subtitle);
            }

            subtitle = HtmlEntities.htmlQuotes(subtitle);
            fsubtitle = subtitle;
            
            List<String> descrL = new ArrayList<String>();
            descrL.add(t.getDescription());
            descrL.add(t.getTextDescription());
            descr = StringUtils.removeLB(StringUtils.joinDescs(descrL, "\u0020"));
            desc_max = descr;
      
            desc_max = HtmlEntities.htmlQuotes(desc_max);
            String br = "<br>";
            // построение всплывающие подсказки.
            if (!StringUtils.isEmpty(subtitle)) {
                br = ".<br><br>";
                desc_hint = "<b>" + HtmlEntities.htmlQuotes(subtitle.trim()) + "</b>" + br;
            } else {
                br = "";
                desc_hint = HtmlEntities.htmlQuotes(subtitle.trim()) + br;
            }

            if (!StringUtils.isEmpty(descr)) {
                br = "<br><br>";
                desc_hint += HtmlEntities.htmlQuotes(desc_max.trim()) + br;
            } else {
                br = "";
                desc_hint += HtmlEntities.htmlQuotes(desc_max.trim()) + br;
            }

     
            /*
             | ------------------------------------------------------------------ 
             |  
             | ------------------------------------------------------------------ 
             */

            // Подготавливаем титул.
            //subtitle = StringUtils.stripStrByPixel(subtitle, '\u0020', 560, 1, "Arial", 16);

            // Подготавливаем дескриптор.
            //descr = StringUtils.stripStrByPixel(descr, '\u0020', (575 - (int) (datelen / 2)), 2, "Arial", 13);

            // Если это первый артикль в секции:
            if (articleCount == 0) {

                // устанавливем CSS класс
                ListOrGrid = "artfirst";

                //подготавливаем титры, хинты и т.д.
                doml = new TitleInfo();
                doml.setTitleRaw(subtitle);
                doml.setHref(HtmlStrUtils.getHostUrl(t.getUrl()));
                doml.setHint(desc_hint);
                listOfDomains.add(doml);                
                           
                    
                // Подготавливаем титул.
                subtitle = StringUtils.srtipStrPxlText(subtitle, "\u0020", "Arial", 17, 530);

                // Подготавливаем дескриптор.
                descr = StringUtils.srtipStrPxlText(descr, "\u0020", "Arial", 14, 1100);
            }


            // Если текущий артикль  больше 1 и, если текущий артикль отмечен как TRUE:
            if ((articleCount > 0) && (article.isShowAsSubsnippet()) && (offset == 0)) {

                // Подготавливаем титул.
                subtitle = StringUtils.srtipStrPxlText(subtitle, "\u0020", "Arial", 17, 260);

                // подготавливаем дескриптор
                descr = StringUtils.srtipStrPxlText(descr, "\u0020", "Arial", 14,  460);

                // Прячем подтитул с сылкой.                  
                sublink = "hide";

                // Устанавливаем CSS стиль для ссылки.
                a_nod = "nod";

                // Отображаем артикль в столбец.
                ListOrGrid = "artgrid";
            }

            if ((articleCount > 0) && (article.isShowAsSubsnippet()) && offset > 0) {
                ListOrGrid = "article";
                HideOrShow = "display: none";
                articleTitle = "artall";

                subtitle = StringUtils.srtipStrPxlText(subtitle, "\u0020", "Arial", 17, 530);

                descr = StringUtils.srtipStrPxlText(descr, "\u0020", "Arial", 14, 1100);
                
                sublink = "hide";
                if (HtmlStrUtils.UrlIsEqual(domenUrl, t.getUrl())) {
                } else {
                    url = HtmlStrUtils.getHostUrl(t.getUrl());
                    if (!(HtmlStrUtils.getOtherDomain(domenUrl, t.getUrl())).isEmpty()) {
                      doml = new TitleInfo();
                      doml.setTitleRaw(subtitle);
                      doml.setHref(HtmlStrUtils.getHostUrl(t.getUrl()));
                      doml.setHint(desc_hint);
                      listOfDomains.add(doml);
                    }
                }

                //descr = StringUtils.stripStrByPixel(descr, '\u0020', (600-(int)datelen), 1, "Arial", 12);
            }

            // Если это артикли, которые следуют после артиклей с рангом повыше
            if (articleCount > articleFilteredCount) {
                ListOrGrid = "article";
                HideOrShow = "display: none";
                articleTitle = "artall";

                subtitle = StringUtils.srtipStrPxlText(subtitle, "\u0020", "Arial", 17, 530);
                descr = StringUtils.srtipStrPxlText(descr, "\u0020", "Arial", 14, 570);
                
                sublink = "hide";
                if (HtmlStrUtils.UrlIsEqual(domenUrl, t.getUrl())) {
                } else {
                    url = HtmlStrUtils.getHostUrl(t.getUrl());
                    if (!(HtmlStrUtils.getOtherDomain(domenUrl, t.getUrl())).isEmpty()) {
                        doml = new TitleInfo();
                        doml.setTitleRaw(subtitle);
                        doml.setHref(HtmlStrUtils.getHostUrl(t.getUrl()));
                        doml.setHint(desc_hint);
                        listOfDomains.add(doml);
                    }
                }

                //descr = StringUtils.stripStrByPixel(descr, '\u0020', (580-(int)datelen), 1, "Arial", 12);
            }


            if ((listOfTitles.size() < 5) && (!article.isShowAsSubsnippet())) {
                tti = new TitleInfo();
                tti.setTitleRaw(fsubtitle);
                tti.setHref(t.getUrl());
                tti.setHint(desc_hint);
                tti.setLinkId(t.getLinkId());
                listOfTitles.add(tti);
            }



            //------------------------------------------------------------------



            //desc_max = descr + descr2; 
            //desc_min = StringUtils.stripStrByPixel(descr, '\u0020', 570, 2, "Arial", 13);
            desc_hint = HtmlStrUtils.markRegWords(StringUtils.capitalize(desc_hint), query);
            desc_min = HtmlStrUtils.markRegWords(StringUtils.capitalize(descr), query);
            subtitle = HtmlStrUtils.markRegWords(StringUtils.capitalize(subtitle), query);


    %>
    <article class="<%=ListOrGrid%>" 
             style="<%=HideOrShow%>" 
             data-name="art<%=articleCount%>">
        <h3 class="<%=articleTitle%>" >
            <a class="<%=a_nod%>" data-toggle="pop-art" title="<%=desc_hint%>" href="<%="http://" + url%>"><%=subtitle%></a>
            <div style="display:none"><%=article.getFirstLink().getLinkId()%></div>
        </h3> 
        <span class="artsubt <%=sublink%>">
            <span style="color: green"><%=HtmlStrUtils.markRegWords(url, query)%></span>
        </span> 
        <span class="am">            
            <%
                String html = "&nbsp;-&nbsp;";

                if (articleCount == 0) {
                    html += "<span class=\"mr\"><a href=\"" + "http://" + domain + "\">More results</a></span>";
                    html += "<span class=\"sep\">&nbsp;-&nbsp;</span>";
                    html += "<span class='analitic' title=\"" + analitic + "\"><a href=''>Similar</a></span>";
                    html += "<span class=\"sep\">&nbsp;-&nbsp;</span>";
                    html += "<span class='analitic' title=\"" + analitic + "\"><a href=''>Analitics</a></span>";
                    // html += "<span class=\"sep\">&nbsp;-&nbsp;</span>";
                    // html += "<span class=\"mr\"><a href=\"\">Block this site</a></span>";
                    out.print(html);
                }
            %> 
        </span>
        <p data-descmin="<%=desc_min%>" data-descmax="<%=desc_max%>" title="" >
            <span style="color: grey"><%=date%></span>     
            <%=desc_min%>
        </p>
    </article>
    <% }
        String str = "";
        String html = "";        
        
        ttl = StringUtils.getSubTitles(listOfTitles, 570, 1, "|", query);
        for(String tt : ttl){
          str += tt;
        }
     
        if (!str.isEmpty()) {
            html += "<div class=\"ptitles " + SecFooterLink + "\">";
            html += str;
            html += "</div>";
        }
        out.print(html);
    %>
</section>
<% sectionCount++;
        }
    }%>