<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>


<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/videos.css"/>

<c:set var="videoList" value="${requestScope.videos}"/>
<c:if test="${not empty videoList}">
  <div class="videos-filter">
    <ul class="videos-list">
      <c:forEach items="${videoList}" var="video" varStatus="videos_status">
        <li class="video-item">
          <a href="${video.link}">
            <img class="lazy PreviewVideo" data-original="${video.thumbnail}" alt="${iz:ucwords(video.title, ' ')}" alt="image"/>
          </a>
          <div class="title">
            <div class="caption">
              <c:set value="${iz:ucwords(video.title, ' ')}" var="title"/>
              <c:out value="${title}"/>
            </div>
            <div class="link">
              <a href="${video.link}">
                <c:out value="${iz:get_domain(video.link)}"/>
              </a>
            </div>
          </div>
        </li>
      </c:forEach>
    </ul>
  </div>
  <script>
    $(document).ready(function () {
      $("img.lazy").lazyload({
        event: "scrollstop"
      });})
  </script>
</c:if>


