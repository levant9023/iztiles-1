<%--
  Created by IntelliJ IDEA.
  User: efanchik
  Date: 3/3/16
  Time: 10:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<c:url var="main_url" value="${baseURL}">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="images_url" value="images.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="faves_url" value="buttons.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="videos_url" value="videos.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="map_url" value="http://open.mapquest.com/">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="news_url" value="news.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="blogs_url" value="blogs_magazines.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="forums_url" value="forum.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="shopping_url" value="shopping.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="health_url" value="health.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="food_url" value="food_recipes.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="kids_url" value="kids.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="music_url" value="music.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="fun_url" value="/fun.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="pets_url" value="/pets.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="politics_url" value="/politics.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="cars_url" value="/cars.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="science_url" value="/science.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="seniors_url" value="/seniors.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="shopping_url" value="/shopping.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="sports_url" value="/sports.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="tech_url" value="/tech.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="travel_url" value="/travel.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="books_url" value="/books.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="art_url" value="/art.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="books_url" value="/books.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="business_url" value="/business.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="celebrity_url" value="/celebrity.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="craft_url" value="/craft.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="design_url" value="/design.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="education_url" value="/education.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<c:url var="family_url" value="/family.html">
  <c:if test="${param.q}">
    <c:param name="q" value="${param.q}"/>
  </c:if>
</c:url>

<side id="left-navigation">
  <div id="lnav-wrapper">
    <ul id="left-nav-default">
      <li id="all-menu-item">
        <a href="<c:out value='${main_url}'/>">Main</a>
      </li>
      <li id="news-menu-item">
        <a href="<c:out value='${news_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/news.svg" alt="image"/></span>
          <span>News</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="image-menu-item">
        <a href="<c:out value='${images_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/images.svg" alt="image"/></span>
          <span>Images</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="videos-menu-item">
        <a href="<c:out value='${videos_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/videos.svg" alt="image"/></span>
          <span>Videos</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="maps-menu-item">
        <a href="<c:out value='${map_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/maps.svg" alt="image"/></span>
          <span>Maps</span>
        </a>
        <span class="badge"></span>
      </li>
    </ul>
    <ul id="left-nav-01" class="list-unstyled">
      <%--<li id="blogs-menu-item">
        <a href="<c:out value='${blogs_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/blogs_magazines.svg" alt="image"/></span>
          <span>Blogs</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="tech-menu-item">
        <a href="<c:out value='${tech_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/tech.svg" alt="image"/></span>
          <span>Technology</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="business-menu-item">
        <a href="<c:out value='${business_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/business.svg" alt="image"/></span>
          <span>Business</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="sports-menu-item">
        <a href="<c:out value='${sports_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/sports.svg" alt="image"/></span>
          <span>Sports</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="cars-menu-item">
        <a href="<c:out value='${cars_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/cars.svg" alt="image"/></span>
          <span>Cars</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="travel-menu-item">
        <a href="<c:out value='${travel_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/travel.svg" alt="image"/></span>
          <span>Travel</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="food-menu-item">
        <a href="<c:out value='${food_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/food_recipes.svg" alt="image"/></span>
          <span>Food & Recipes</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="health-menu-item">
        <a href="<c:out value='${health_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/health.svg" alt="image"/></span>
          <span>Health</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="shopping-menu-item">
        <a href="<c:out value='${shopping_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/shopping.svg" alt="image"/></span>
          <span>Shopping</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="science-menu-item">
        <a href="<c:out value='${science_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/science.svg" alt="image"/></span>
          <span>Science</span>
        </a>
        <span class="badge"></span>
      </li>
      <li id="politics-menu-item">
        <a href="<c:out value='${politics_url}'/>">
          <span class="img"><img src="${baseURL}/resources/img/nav/politics.svg" alt="image"/></span>
          <span>Politics</span>
        </a>
        <span class="badge"></span>
      </li>--%>

        <li><a href="/blogs_magazines.html"><img src="/resources/img/nav/blogs_magazines.svg"><span>blogs</span></a></li>
        <li><a href="/tech.html"><img src="/resources/img/nav/tech.svg"><span>technology</span></a></li>
        <li><a href="/business.html"><img src="/resources/img/nav/business.svg"><span>business</span></a></li>
        <li><a href="/sports.html"><img src="/resources/img/nav/sports.svg"><span>sports</span></a></li>
        <li><a href="/cars.html"><img src="/resources/img/nav/cars.svg"><span>cars</span></a></li>
        <li><a href="/travel.html"><img src="/resources/img/nav/travel.svg"><span>travel</span></a></li>
        <li><a href="/food_recipes.html"><img src="/resources/img/nav/food_recipes.svg"><span>food</span></a></li>
        <li><a href="/health.html"><img src="/resources/img/nav/health.svg"><span>health</span></a></li>
        <li><a href="/shopping.html"><img src="/resources/img/nav/shopping.svg"><span>shopping</span></a></li>
        <li><a href="/science.html"><img src="/resources/img/nav/science.svg"><span>science</span></a></li>
        <li><a href="/fun.html"><img src="/resources/img/nav/fun.svg"><span>fun</span></a></li>
        <li><a href="/pets.html"><img src="/resources/img/nav/pets.svg"><span>pets</span></a></li>
        <li><a href="/politics.html"><img src="/resources/img/nav/politics.svg"><span>politics</span></a></li>
        <li><a href="/celebrity.html"><img src="/resources/img/nav/celebrity.svg"><span>celebrity</span></a></li>
        <li><a href="/movies.html"><img src="/resources/img/nav/movies.svg"><span>movies</span></a></li>
        <li><a href="/music.html"><img src="/resources/img/nav/music.svg"><span>music</span></a></li>
        <li><a href="/lifestyle.html"><img src="/resources/img/nav/lifestyle.svg"><span>lifestyle</span></a></li>
        <li><a href="/style_fashion.html"><img src="/resources/img/nav/style_fashion.svg"><span>style</span></a></li>
        <li><a href="/men.html"><img src="/resources/img/nav/men.svg"><span>men</span></a></li>
        <li><a href="/women.html"><img src="/resources/img/nav/women.svg"><span>women</span></a></li>
        <li><a href="/teachers.html"><img src="/resources/img/nav/teachers.svg"><span>teachers</span></a></li>
        <li><a href="/education.html"><img src="/resources/img/nav/education.svg"><span>education</span></a></li>
        <li><a href="/books.html"><img src="/resources/img/nav/books.svg"><span>books</span></a></li>
        <li><a href="/craft.html"><img src="/resources/img/nav/craft.svg"><span>crafts</span></a></li>
        <li><a href="/family.html"><img src="/resources/img/nav/family.svg"><span>family</span></a></li>
        <li><a href="/gifts.html"><img src="/resources/img/nav/gifts.svg"><span>gifts</span></a></li>
        <li><a href="/home.html"><img src="/resources/img/nav/home.svg"><span>home</span></a></li>
        <li><a href="/jobs.html"><img src="/resources/img/nav/jobs.svg"><span>jobs</span></a></li>
        <li><a href="/kids.html"><img src="/resources/img/nav/kids.svg"><span>kids</span></a></li>
        <li><a href="/law.html"><img src="/resources/img/nav/law.svg"><span>law</span></a></li>
        <li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>teens</span></a></li>
        <li><a href="/real_estate.html"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
        <li><a href="/seniors.html"><img src="/resources/img/nav/seniors.svg"><span>seniors</span></a></li>
        <li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>teens</span></a></li>
        <li><a href="/tips_tutorials.html"><img src="/resources/img/nav/tips_tutorials.svg"><span>tips</span></a></li>
        <li><a href="/tools.html"><img src="/resources/img/nav/tools.svg"><span>tools</span></a></li>
        <li><a href="/tv.html"><img src="/resources/img/nav/tv.svg"><span>tv</span></a></li>
    </ul>
    <span id="lnav_btn_1" class="pinned_list more_sidebar more_sidebar_1">More</span>
  </div>
</side>

<script>
$(document).ready(function(){
  /*var url = window.location.href;
  var arr = url.split('=');
  var myoutput = arr && arr[1] || 0;
  if(myoutput.length > 0){
    $('#left-nav-01').css("width","320px");
  }*/
    $('body').on('click', '.more_sidebar_1', function(){
     $('#left-nav-01').toggleClass('all_cats');
     $(this).html($(this).html() == 'More' ? 'Less' : 'More');
    });
    $('body').on('click', '.more_sidebar_2', function(){
     $('#left-nav-02').toggleClass('all_cats');
     $(this).html($(this).html() == 'More' ? 'Less' : 'More');
    });
  $('body').on('mouseenter', '#left_sidebar', function(){
    $(this).css('overflow-y', 'auto');
    $('#left_sidebar .accordion li i.fa-chevron-down').show();
  });
  $('body').on('mouseleave', '#left_sidebar', function(){
    $(this).css('overflow-y', 'hidden');
    $('#left_sidebar .accordion li i.fa-chevron-down').hide();
  });
});
</script>